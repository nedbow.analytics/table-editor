const en = {
  common: {
    rowsCountingText: 'Selected rows',
    csvDownload: 'CSV',
    jsonDownload: 'JSON',
    formatIsNotAllowed: 'Could not determine the data format',
    dataIsNotJson: 'Data is not JSON',
    dataSuccess: 'The data was successfully uploaded. The data in "Table" tab'
  },
  tab: {
    table: 'Table',
    data: 'Data',
    upload: 'Upload File'
  },
  button: {
    delete: 'Remove',
    addRow: '+ Add row',
    downloadTable: 'Download table',
    upload: 'Upload',
    update: 'Update table'
  },
  components: {
    form: {
      textareaPlaceholder: 'JSON format'
    },
    loader: {
      description: 'Upload a file from your computer. You may select only CSV or JSON file extension.',
      fileInputPlaceholder: 'Select file'
    },
    table: {
      moveUp: 'Move Up',
      moveDown: 'Move Down'
    }
  },
  errors: {
    incorrectFileType: 'Incorrect file type'
  }
}

export default en
