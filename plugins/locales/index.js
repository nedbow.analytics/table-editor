// Require language packages
// TODO: maby use autoload lang packages?

const locales = {
  'ru_RU': require('./ru_RU.js').default,
  'en_EN': require('./en_EN.js').default
}

export default class i18n {
  /**
   * Constructor
   */
  constructor () {
    this.localeDefault = 'en_EN'
  }

  /**
   * Vue plugin initializator
   * @param { Vue } Vue - Vue inctance
   * @param { Object } options - optional argument with instalation params
   */
  install (Vue, options) {
    let self = this
    let locale = (options && options.locale) ? options.locale : this.localeDefault
    this.setLocale(locale)
    // Add globally
    Vue.prototype.$t = function (propsString) {
      return self.print(propsString)
    }
  }

  /**
   * Return current locale value
   */
  getLocale () {
    return this.locale
  }

  /**
   * Set localization
   * @param { String } locale - string of new locale
   */
  setLocale (locale) {
    if (locale !== this.getLocale() && locales.hasOwnProperty(locale)) {
      this.locale = locale
    } else {
      this.locale = this.localeDefault
    }
  }

  /**
   * Method return locale value by property string from locales object
   * @param { String } propsString - string
   */
  print (propsString) {
    let propsArray = propsString.split('.')
    if (typeof propsString === 'string' && propsArray.length) {
      let message = this._getObjectValue(propsArray, locales[this.locale])
      if (message != null && typeof message === 'string') {
        return message
      }
    }
    return propsString
  }

  /**
   * Protected method which can array items using like object properties
   * For example string "['a', 'b', 'c']" return value from object like obj['a']['b']['c']
   * @param { Array } propsArray - array with consistent properties
   * @param { Object } source - source object
   */
  _getObjectValue (propsArray, source) {
    const get = (props, object) =>
      props.reduce((xs, x) =>
        (xs && xs[x]) ? xs[x] : null, object)

    return get(propsArray, source)
  }
}
