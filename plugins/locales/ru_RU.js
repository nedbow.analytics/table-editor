const ru = {
  common: {
    rowsCountingText: 'Выбрано строк',
    csvDownload: 'В формате CSV',
    jsonDownload: 'В формате JSON',
    formatIsNotAllowed: 'He удалось определить формат данных',
    dataIsNotJson: 'Данные не являются JSON, либо не соответствуют заданному формату',
    dataSuccess: 'Данные успешно загружены. Для просмотра перейдите во вкладку "Таблица"'
  },
  tab: {
    table: 'Таблица',
    data: 'Данные',
    upload: 'Загрузить файл'
  },
  button: {
    delete: 'Удалить',
    addRow: '+ Добавить строку',
    downloadTable: 'Скачать таблицу',
    upload: 'Загрузить',
    update: 'Обновить таблицу'
  },
  components: {
    form: {
      textareaPlaceholder: 'Данные в формате JSON'
    },
    loader: {
      description: 'Загрузите файл в формате JSON или CSV с вашего компьютера',
      fileInputPlaceholder: 'Выберите файл'
    },
    table: {
      moveUp: 'Переместить вверх',
      moveDown: 'Переместить вниз'
    }
  },
  errors: {
    incorrectFileType: 'Формат загружаемого файла не поддерживается'
  }
}

export default ru
