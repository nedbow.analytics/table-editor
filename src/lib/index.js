export const ALLOWED_DATA_TYPES = ['JSON', 'CSV']
export const DEFAULT_TABLE_COLUMNS = ['Name', 'Value']
export const DEFAULT_ROW = { name: 'name', value: 'value' }

/**
 * ID generator
 * @param { String } prefix - prefix
 */
export function genId (prefix = '') {
  const suffix = Math.random().toString(36).substring(6)

  if (prefix) {
    return `${prefix}-${suffix}`
  }

  return suffix
}

/**
 * Convert data object to string by seted format
 * @param { Object } data
 * @param { String } format - needed format
 */
export function dataToString (data, format) {
  format = format.toLowerCase()

  if (format === 'json') {
    return JSON.stringify(data)
  } else if (format === 'csv') {
    let rows = data.rows.map(row => {
      return Object.values(row).join(';')
    })

    return rows.join('\n')
  }
}
