import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'

// Import own i18n plugin
import Localization from '../plugins/locales'

// 3-rd party modules including
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Project use bootstrap
Vue.use(BootstrapVue)

// Project use localization
Vue.use(new Localization(), { locale: 'ru_RU' })

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
