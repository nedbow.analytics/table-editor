import Vue from 'vue'
import Router from 'vue-router'
import TableEditor from '@/components/TableEditor.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: TableEditor
    }
  ]
})
