// Because the project is small this solution is normal
// The next project iteration should use Vuex
export const Store = {
  state: {
    source: null,
    sourceString: '',
    defaultType: 'json',
    editableCell: null
  },
  /**
   * Merge changes from table
   */
  sourceMergeRows: function (newRows) {
    this.state.source.rows = newRows
    let newSourceString = JSON.stringify(this.state.source)
    if (this.state.sourceString !== newSourceString) {
      this.setSourceString(newSourceString)
    }
  },
  /**
   * Set source string
   */
  setSourceString: function (string) {
    this.state.sourceString = string
  }
}
