import Loader from '@/components/Loader.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Localization from '../../plugins/locales'
import BootstrapVue from 'bootstrap-vue'

const localVue = createLocalVue()
localVue.use(new Localization())
localVue.use(BootstrapVue)

const wrapper = shallowMount(Loader, {
  localVue
})

describe('Loader.vue', () => {
  it('JSON file extention and filetype is valid', () => {
    let file = new File([''], 'userData.json', { type: 'text/json' })
    expect(wrapper.vm.isValidFile(file)).toBeTruthy()
  })
  it('CSV file extention and filetype is valid', () => {
    let file = new File([''], 'userData.csv', { type: 'text/csv' })
    expect(wrapper.vm.isValidFile(file)).toBeTruthy()
  })
  it('Another file extention and filetype is not valid', () => {
    let file = new File([''], 'userData.txt', { type: 'text/plain' })
    expect(wrapper.vm.isValidFile(file)).toBe(false)
  })
})
