import TableEditor from '@/components/TableEditor.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Localization from '../../plugins/locales'
import BootstrapVue from 'bootstrap-vue'

const localVue = createLocalVue()
localVue.use(new Localization())
localVue.use(BootstrapVue)

const mockJsonString = '{"rows":[{"name":"name1","value":"value1"}]}'
const mockCsvString = 'name1;value1;\nname2;value2;'

const wrapper = shallowMount(TableEditor, {
  localVue
})

describe('TableEditor.vue', () => {
  it('should correctly parse JSON string', () => {
    wrapper.vm.setupData(mockJsonString, 'json')
    expect(wrapper.vm.sourceString).toBe(mockJsonString)
  })
  it('should correctly parse CSV string', () => {
    wrapper.vm.setupData(mockCsvString, 'csv')
    expect(wrapper.vm.sourceString).toBe(mockJsonString)
  })
  it('data does not match the scheme', () => {
    expect(wrapper.vm.isValidJSON(JSON.parse('[{"name":"name1","value":"value1","somekey":"somevalue"}]'))).toBe(false)
  })
})
